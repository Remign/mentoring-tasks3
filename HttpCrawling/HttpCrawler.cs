﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CsQuery;

namespace HttpCrawling
{
  public delegate Task SaveHtmlEventHandler(string path, string content);

  public delegate Task SaveByteContentEventHandler(string path, byte[] byteArray);
  public class HttpCrawler
  {
    private const string HttpRegexString = @"^https?:\/\/";
    private readonly int _maxDeep;

    public HttpCrawler(int maxDeep = 10)
    {
      _maxDeep = maxDeep;
    }

    public event SaveHtmlEventHandler SaveHtmlEvent;

    public event SaveByteContentEventHandler SaveByteContentEvent;

    public async Task Execute(string address, int deep = 0)
    {
      if (deep >= _maxDeep) return;

      try
      {
        var client = new HttpClient();

        var response = await client.GetAsync(address);

        response.EnsureSuccessStatusCode();

        Console.WriteLine("{0} processed", address);

        var contentType = response.Content.Headers.ContentType.MediaType;

        if (contentType == "text/html")
        {
          var html = await response.Content.ReadAsStringAsync();

          var path = this.PreparePath(address);

          await this.OnSaveHtmlEvent(path, html);

          var cq = CQ.Create(html);

          await this.ProcessImg(cq, address);
          await this.ProcessHref(cq, address, deep);
        }
        else if (contentType == "image/jpeg" || contentType == "application/pdf")
        {
          var byteArray = await response.Content.ReadAsByteArrayAsync();

          var path = this.PreparePath(address);

          await this.OnSaveByteContentEvent(path, byteArray);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine("{0} {1}", address, e.Message);
      }

    }

    private async Task ProcessHref(CQ cq, string address, int deep)
    {
      var newTasks = new List<Task>();

      foreach (var obj in cq.Find("a"))
      {
        var newAddress = obj.GetAttribute("href");

        if (newAddress.Trim() == "#") continue;

        if (Regex.IsMatch(newAddress, HttpRegexString) == false &&
            Regex.IsMatch(newAddress, address) == false)
        {
          newAddress = address + newAddress;
        }

        newTasks.Add(this.Execute(newAddress, ++deep));
      }

      Task.WaitAll(newTasks.ToArray());
    }

    private async Task ProcessImg(CQ cq, string address)
    {
      var newTasks = new List<Task>();

      foreach (var obj in cq.Find("img"))
      {
        var newAddress = obj.GetAttribute("src");

        if (newAddress.Trim() == "#") continue;

        if (Regex.IsMatch(newAddress, HttpRegexString) == false &&
            Regex.IsMatch(newAddress, address) == false)
        {
          newAddress = address + newAddress;
        }

        newTasks.Add(this.Execute(newAddress));
      }

      Task.WaitAll(newTasks.ToArray());
    }

    private string PreparePath(string address)
    {
      var path = Regex.Replace(address, HttpRegexString, string.Empty);
      path = Regex.Replace(path, @"|:<>?", string.Empty);
      path = Regex.Replace(path, "/", @"\");

      return path;
    }

    protected async virtual Task OnSaveHtmlEvent(string path, string content)
    {
      var handler = SaveHtmlEvent;
      if (handler != null)
      {
        await handler(path, content);
      }
    }

    protected async virtual Task OnSaveByteContentEvent(string path, byte[] byteArray)
    {
      var handler = SaveByteContentEvent;
      if (handler != null)
      {
        await handler(path, byteArray);
      }
    }
  }
}
