﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogParser
{
  class Program
  {
    static void Main(string[] args)
    {
      //MsUtil is located in => Interop.MSUtil.dll

      var logQuery = new MSUtil.LogQueryClass();
      var input = new MSUtil.COMEventLogInputContextClass();

      

      //var resultDataSet = logQuery.Execute(
      //  "SELECT TimeGenerated, SourceName, EventID, Message " +
      //  "FROM Application WHERE SourceName = 'ESENT' " +
      //  "ORDER BY TimeGenerated DESC",
      //  input);

      //var resultDataSet = logQuery.Execute(
      //  "SELECT TimeGenerated, SourceName, EventID, Message, EventTypeName " +
      //  "FROM Application WHERE EventTypeName = 'Error event'",
      //  input);

      Console.WriteLine("Error types and theirs number:");

      var typesResultDataSet = logQuery.Execute(
        "SELECT EventTypeName, count(EventTypeName) as Number " +
        "FROM Application group by EventTypeName",
        input);

      while (!typesResultDataSet.atEnd())
      {
        var record = typesResultDataSet.getRecord();
        Console.WriteLine("{0} : {1}", record.getValue("EventTypeName"), record.getValue("Number"));

        typesResultDataSet.moveNext();
      }

      Console.WriteLine("Errors...:");

      var resultDataSet = logQuery.Execute(
        "SELECT TimeGenerated, SourceName, EventID, Message " +
        "FROM Application where EventTypeName = 'Error event'",
        input);

      while (!resultDataSet.atEnd())
      {
        var record = resultDataSet.getRecord();
        Console.WriteLine("{0} : {1}", record.getValue(0), record.getValue("Message"));

        resultDataSet.moveNext();
      }
    }
  }
}
