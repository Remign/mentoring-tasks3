﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MvcMusicStore.Infrastructure;
using NLog;
using PerformanceCounterHelper;

namespace MvcMusicStore
{
  public class MvcApplication : System.Web.HttpApplication
  {
    public static readonly CounterHelper<Counters> CounterHelper = PerformanceHelper.CreateCounterHelper<Counters>("Test counter for music store");

    private readonly ILogger _logger;

    public MvcApplication()
    {
      _logger = LogManager.GetCurrentClassLogger();
    }


    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);

      //DependencyResolver

      CounterHelper.RawValue(Counters.GoToHome, 0);
      CounterHelper.RawValue(Counters.LogIn, 0);
      CounterHelper.RawValue(Counters.LogOff, 0);

      _logger.Info("Application started");
    }
  }
}
