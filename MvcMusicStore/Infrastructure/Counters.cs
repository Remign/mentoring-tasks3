﻿using System.Diagnostics;
using PerformanceCounterHelper;

namespace MvcMusicStore.Infrastructure
{
  [PerformanceCounterCategory("MvcMusicStore", PerformanceCounterCategoryType.MultiInstance, "MvcMusicStoreInfoFieldValue")]
  public enum Counters
  {
    [PerformanceCounter("Go to Home page counter", "Go to home page", PerformanceCounterType.NumberOfItems32)]
    GoToHome,
    [PerformanceCounter("Sucess log in counter", "Sucess log in", PerformanceCounterType.NumberOfItems32)]
    LogIn,
    [PerformanceCounter("Sucess log off counter", "Sucess log off", PerformanceCounterType.NumberOfItems32)]
    LogOff
  }
}