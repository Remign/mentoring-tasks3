﻿using System;
using System.IO;
using System.Threading.Tasks;
using HttpCrawling;

namespace Task_Http
{
  public class CrawlerClient
  {
    private const string FolderName = "CrawlerOutput";
    private const string HtmlExtension = ".html";
    private readonly string _currentDirectory = Environment.CurrentDirectory;
    private readonly string _startAddress;
    private readonly HttpCrawler _httpCrawler;

    public CrawlerClient(HttpCrawler httpCrawler, string startAddress)
    {
      _httpCrawler = httpCrawler;
      _startAddress = startAddress;
      Directory.CreateDirectory(this.StartPath);

      _httpCrawler.SaveHtmlEvent += this.SaveHtmlContent;
      _httpCrawler.SaveByteContentEvent += this.SaveByteContent;
    }

    public async Task Execute()
    {
      await _httpCrawler.Execute(_startAddress);
    }

    public async Task SaveHtmlContent(string path, string content)
    {
      var filePath = this.StartPath + @"\" + path + HtmlExtension;
     
      this.DeepCreateDirectory(filePath);

      using (var file = new StreamWriter(filePath))
      {
        await file.WriteAsync(content);
      }
    }

    public async Task SaveByteContent(string path, byte[] byteArray)
    {
      var filePath = this.StartPath + @"\" + path;

      this.DeepCreateDirectory(filePath);

      using (var file = new BinaryWriter(new FileStream(filePath, FileMode.Create)))
      {
        file.Write(byteArray);
      }
    }

    private void DeepCreateDirectory(string filePath)
    {
      var directoryName = Path.GetDirectoryName(filePath);

      if (directoryName != null)
      {
        Directory.CreateDirectory(directoryName);
      }
    }

    public string StartPath
    {
      get
      {
        return _currentDirectory + @"\" + FolderName;
      }
    }
  }
}