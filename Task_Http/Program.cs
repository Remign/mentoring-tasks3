﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HttpCrawling;

namespace Task_Http
{
  class Program
  {
    static void Main(string[] args)
    {
    
      var startAddress = "http://www.mycity.by";
      //var startAddress = "http://e-journal.spa.msu.ru/uploads/vestnik/2011/vipusk__28._sentjabr_2011_g./problemi_upravlenija_teorija_i_praktika/miroshnikov_phillipchuk.pdf";

      var httpCrawler = new HttpCrawler();

      var crawlerClient = new CrawlerClient(httpCrawler, startAddress);

      Task.WaitAll(crawlerClient.Execute());

    }


  }
}
